<?php
function fibonacci($val){

	if ($val < 0)
	{
	return ('No negative values');
	}
	elseif ($val == 0)
	{
	    return ('0');
	}
	elseif ($val == 1)
	{
	    return ('1');
	}
	else 
	{
	    $sum = fibonacci($val-1)+fibonacci($val-2); 
	    return ($sum);
	}

}

echo '<pre>' . fibonacci(5) . '</pre>'; 
echo '<pre>' . fibonacci(10) . '</pre>'; 
echo '<pre>' . fibonacci(1) . '</pre>'; 
          
?>  

